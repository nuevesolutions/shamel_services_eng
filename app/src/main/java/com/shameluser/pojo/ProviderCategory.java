package com.shameluser.pojo;

/**
 * Created by user145 on 10/4/2017.
 */
public class ProviderCategory {

    private String category_name="";
    private String hourly_rate="";
    private String quick_pitch = "";

    public String getQuick_pitch() {
        return quick_pitch;
    }

    public void setQuick_pitch(String quick_pitch) {
        this.quick_pitch = quick_pitch;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }
}
