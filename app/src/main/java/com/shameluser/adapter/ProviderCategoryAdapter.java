package com.shameluser.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shameluser.R;
import com.shameluser.pojo.ProviderCategory;
import com.shameluser.utils.CurrencySymbolConverter;
import com.shameluser.utils.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user145 on 10/4/2017.
 */
public class ProviderCategoryAdapter extends BaseAdapter {

    private ArrayList<ProviderCategory> cat_list_item;
    ArrayList<String> listItems;
    private LayoutInflater mInflater;
    private Context context;
    private SessionManager sessionManager;



    public ProviderCategoryAdapter(Context context, ArrayList<String> listItems, ArrayList<ProviderCategory> cat_list_item) {

        this.context=context;
        this.listItems=listItems;
        this.cat_list_item=cat_list_item;
        mInflater = LayoutInflater.from(context);
        sessionManager=new SessionManager(context);

    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public class ViewHolder {
     private TextView category;
     private TextView hourly_rate;
     private TextView txt_sub_cat;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.provider_category_list, parent, false);
            holder = new ViewHolder();
            holder.category = (TextView) convertView.findViewById(R.id.category);
            holder.hourly_rate = (TextView) convertView.findViewById(R.id.hourly_rate);
            holder.txt_sub_cat = (TextView) convertView.findViewById(R.id.txt_sub_cat);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> aAmountMap = sessionManager.getWalletDetails();
        String aCurrencyCode = aAmountMap.get(SessionManager.KEY_CURRENCY_CODE);
        final String myCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(aCurrencyCode);

        holder.category.setText(cat_list_item.get(position).getCategory_name());
        holder.hourly_rate.setText(myCurrencySymbol+cat_list_item.get(position).getHourly_rate()+"/"+"hr");
        Log.e("category name","cat"+cat_list_item.get(position).getQuick_pitch().replace("\\n", "<br/>"));
        holder.txt_sub_cat.setText(cat_list_item.get(position).getQuick_pitch());
        return convertView;
    }
}